# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    test_criterion.mk                                  :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/08/08 18:27:52 by mbeilles          #+#    #+#              #
#    Updated: 2020/01/23 13:28:44 by mbeilles         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #
-include ./strings.mk

# Ensures defaults so you don't have to.
PATH_TEST ?= tests
BIN_TEST ?= test

ifeq (NO_TEST_FILES,)
# If there is no tests to be compiled without specifing NO_TEST_FILES
# Then there is a problem and the test rule should be aborted.
$(call assert,$(TESTS),Test files are undefined, cannot run tests...)
else
# Gets all tests files for the conversion with the tests entry point
# Then gets all program sources excepts it's entry point
TEST_OBJS := $(patsubst %.c, $(PATH_OBJ)/%.o, $(TESTS)) \
	$(patsubst %.c, $(PATH_OBJ)/%.o, $(filter-out $(PROG_ENTRY_POINT),$(SRCS)))
endif

UNAME := $(shell uname -s)
ifeq ($(UNAME),Darwin)
# Handles user installed criterion with brew
CRIT_CFLAGS ?= -I$(HOME)/.brew/include
CRIT_LDFLAGS ?= -L$(HOME)/.brew/lib -lcriterion
endif
CRIT_LDFLAGS ?= -lcriterion

$(PATH_OBJ)/%.o: %.c 					| $$(@D)/. $(PATH_DEP)/$$(*D)/. $(PATH_DEP)/compile/. $$(LDLIBS) $$(CLIBS)
$(PATH_OBJ)/%.o: %.c $(PATH_DEP)/%.d	| $$(@D)/. $(PATH_DEP)/$$(*D)/. $(PATH_DEP)/compile/. $$(LDLIBS) $$(CLIBS)
	@$(CC) $(CRIT_CFLAGS) $(CFLAGS) -MT $@ -MMD -MP -MF $(PATH_DEP)/$*.Td -c $< -o $@; \
	$(eval ITER:=$(shell echo $$(($(ITER)+1)))) \
	if [ "$$?" != "1" ]; then \
		printf $(COMPILING_OK) ; \
		exit 0; \
	else \
		printf $(COMPILING_KO); \
		exit 2; \
	fi
	@mv -f $(PATH_DEP)/$*.Td $(PATH_DEP)/$*.d &>/dev/null
	@touch $@ # Updating generated because time of modification can be out of sync

test: $(TEST_OBJS) $(CLIBS)
	@printf $(COMPILING_TEST)
	@$(CC) $(CRIT_LDFLAGS) $(LDFLAGS) $(LDLIBS) $(CFLAGS) -o $(BIN_TEST) $^ ; \
		if [ "$$?" != "1" ]; then \
			printf $(MAKING_TEST_SUCCESS); \
			exit 0; \
		else \
			printf $(MAKING_TEST_FAILURE); \
			exit 2; \
		fi
	@chmod +x test
	@printf $(TESTING)
	@./$(BIN_TEST) --list
	@./$(BIN_TEST) --full-stats

.PHONY: test print
