ifeq ($(HOSTTYPE),)
	HOSTTYPE := $(shell uname -m)_$(shell uname -s)
endif

UNAME_S := $(shell uname -s)

ifeq ($(UNAME_S),Linux)
	LDFLAGS+= 
else ifeq ($(UNAME_S),Darwin)
endif

ifeq ($(UNAME_S),Linux)
LDFLAGS+=$(foreach lib, $(SYSLIBS_LINUX), -l$(lib)) $(LDFLAGS_LINUX) -shared
else ifeq ($(UNAME_S),Darwin)
LDFLAGS+=$(foreach lib, $(SYSLIBS_DARWIN), -l$(lib)) $(LDFLAGS_DARWIN) -dynamiclib
endif
LDFLAGS+=$(foreach lib, $(SYSLIBS), -l$(lib))

$(NAME): $(CLIBS) $(OBJS)
	@printf $(START_MSG)
	@printf $(MAKING_PROGRESS)
	@mkdir -p bin/shared
	@$(CC) $(LDFLAGS) $(LDLIBS) $(CFLAGS) -fPIC $(OBJS) -o bin/shared/$(patsubst %.so,%_$(HOSTTYPE).so,$@) -Wl,-rpath,.; \
		if [ "$$?" != "1" ]; then \
			printf $(MAKING_SUCCESS); \
			exit 0; \
		else \
			printf $(MAKING_FAILURE); \
			exit 2; \
		fi
	@ln -fs bin/shared/$(patsubst %.so,%_$(HOSTTYPE).so,$@) $(NAME)
