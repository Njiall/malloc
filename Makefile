# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/04/04 04:17:21 by mbeilles          #+#    #+#              #
#    Updated: 2020/07/23 17:12:01 by mbeilles         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#==============================================================================#
#                                  VARIABLES                                   #
#------------------------------------------------------------------------------#
#                                Customisation                                 #
#==============================================================================#

NAME = libft_malloc.so

NICK = MEM
PROJECT_COLOR = "\033[38;5;26m"
PROJECT_COLOR_ALT = "\033[38;5;21m"

CC = clang

#==============================================================================#
#                                   Sources                                    #
#==============================================================================#

SRCS =	\
		$(PATH_SRC)/malloc.c												\
		$(PATH_SRC)/realloc.c												\
		$(PATH_SRC)/free.c													\
		\
		$(PATH_SRC)/show_mem.c												\
		\
		$(PATH_SRC)/utils/heap_find_fit.c									\
		$(PATH_SRC)/utils/heap_allocate.c									\
		$(PATH_SRC)/utils/heap_push_front.c									\
		\
		$(PATH_SRC)/utils/ctx_init_runtime.c								\
		\
		$(PATH_SRC)/utils/block_new.c										\
		\
		$(PATH_SRC)/print/print_heap_zone.c									\

INC = \
	   $(PATH_INC)			\

LIBS =	\
		./libft \

SYSLIBS =	 \

SYSLIBS_LINUX =	\

TESTS =	\

#==============================================================================#
#                                   Paths                                      #
#==============================================================================#

PATH_SRC := src
PATH_INC := include
PATH_OBJ := .obj
PATH_DEP := .dep

#==============================================================================#
#                                 Compilation                                  #
#==============================================================================#

LDLIBS = \

CLIBS = $(foreach dep, $(LIBS), $(dep)/$(notdir $(dep)).a) \

LDFLAGS = -Llibft -lft
LDFLAGS_DARWIN =
LDFLAGS_LINUX = -fuse-ld=lld # Don't forget to install lld as ld don't use .a files

CFLAGS = $(foreach inc, $(INC), -I$(inc)) \
		 $(foreach dep, $(LIBS), -I$(dep)/$(PATH_INC)) \
		 $(foreach dep, $(LDLIBS), -I$(dep)/$(PATH_INC)) \

#==============================================================================#
#                                   Various                                    #
#==============================================================================#

SHELL = bash

#==============================================================================#
#                             Variables Customizers                            #
#==============================================================================#

OBJS := $(patsubst %.c, $(PATH_OBJ)/%.o, $(SRCS))
DEPS := $(patsubst %.c, $(PATH_DEP)/%.d, $(SRCS))

#==============================================================================#
#                                    Rules                                     #
#==============================================================================#

include makefiles/libso.mk
include makefiles/depend.mk
include makefiles/strings.mk
