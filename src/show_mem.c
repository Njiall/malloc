/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   show_mem.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/19 10:30:08 by mbeilles          #+#    #+#             */
/*   Updated: 2020/07/27 12:47:17 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc_internal.h"
#include "libft.h"

#include <stdio.h>
#include <unistd.h>

#include <math.h>

static inline size_t	_get_used_bins(
		const t_bin * const bins,
		const size_t size
)
{
	size_t				used = 0;

	for (size_t i = 0; i < size; ++i)
		if (bins[i].heap)
			++used;
	return (used);
}

static inline void		_write_padding(
		const uint8_t pad
)
{
	static const char	s[256] = {[0 ... 255] = ' '};

	write(1, s, pad);
}

static inline void		_write_span(
		const uint8_t pad,
		const uintptr_t start,
		const uintptr_t used
)
{
	char *fmt;
	_write_padding(pad);
	write(1, "\e[2m[\e[34m0x\e[0;34m", 19);
	fmt = ft_ultostr(start, 16, false);
	write(1, fmt, ft_strlen(fmt));
	write(1, "\e[0;2m ... \e[34m0x\e[0;34m", 25);
	fmt = ft_ultostr(used, 16, false);
	write(1, fmt, ft_strlen(fmt));
	write(1, "\e[0;2m]\e[0m\n", 12);
}

static inline void		_write_member_ptr(
		const uint8_t pad,
		const char * const name,
		const intptr_t addr,
		const bool newline,
		const bool last
)
{
	_write_padding(pad);
	write(1, "\e[2m.\e[0m", 9);
	write(1, name, ft_strlen(name));
	write(1, "\e[2m: \e[34m0x\e[0;34m", 20);
	char *fmt = ft_ultostr(addr, 16, false);
	write(1, fmt, ft_strlen(fmt));
	if (!last)
		write(1, "\e[0;2m,", 7);
	write(1, "\e[0m\n", 4 + newline);
}

static inline void		_write_member_size(
		const uint8_t pad,
		const char * const name,
		const size_t size,
		const bool newline,
		const bool last
)
{
	_write_padding(pad);
	write(1, "\e[2m.\e[0m", 9);
	write(1, name, ft_strlen(name));
	write(1, "\e[2m: \e[0;35m", 13);
	char *fmt = ft_ultostr(size, 10, false);
	write(1, fmt, ft_strlen(fmt));
		if (!last)
	write(1, "\e[0;2m,", 7);
	write(1, "\e[0m\n", 4 + newline);
}

static inline void		_write_member_str(
		const uint8_t pad,
		const char * const name,
		const char * const str,
		const bool newline,
		const bool last
)
{
	_write_padding(pad);
	write(1, "\e[2m.\e[0m", 9);
	write(1, name, ft_strlen(name));
	write(1, "\e[2m: \e[32m\"\e[0;32m", 19);
	write(1, str, ft_strlen(str));
	write(1, "\e[32;2m\"\e[0;2m,", 14 + !last);
	write(1, "\e[0m\n", 4 + newline);
}
static inline void		_write_member_empty(
		const uint8_t pad,
		const char * const name,
		const bool newline
)
{
	_write_padding(pad);
	write(1, "\e[2m.\e[0m", 9);
	write(1, name, ft_strlen(name));
	write(1, "\e[2m:\e[0m\n", 9 + newline);
}
static inline void		_write_member_bool(
		const uint8_t pad,
		const char * const name,
		const bool value,
		const bool newline,
		const bool last
)
{
	_write_padding(pad);
	write(1, "\e[2m.\e[0m", 9);
	write(1, name, ft_strlen(name));
	write(1, "\e[2m: ", 6);
	if (value)
		write(1, "\e[0;32mtrue\e[0m", 15);
	else
		write(1, "\e[0;31mfalse\e[0m", 16);
	if (!last)
		write(1, "\e[2m,\e[0m", 9);
	if (newline)
		write(1, "\n", 1);
}

static inline void		_write_head(
		const uint8_t pad,
		const char * const name,
		const char * const color,
		const size_t index,
		const bool newline
)
{
	_write_padding(pad);
	write(1, color, ft_strlen(color));
	write(1, name, ft_strlen(name));
	write(1, "\e[0;2m[\e[0;35m", 14);
	char *fmt = ft_ultostr(index, 10, true);
	write(1, fmt, ft_strlen(fmt));
	write(1, "\e[0;2m] = {\e[0m\n", 15 + newline);
}

static inline void		_write_head_range(
		const uint8_t pad,
		const char * const name,
		const char * const color,
		const size_t start,
		const size_t end,
		const bool newline
)
{
	_write_padding(pad);
	write(1, color, ft_strlen(color));
	write(1, name, ft_strlen(name));
	write(1, "\e[0;2m[\e[0;35m", 14);
	{
		char *fmt = ft_ultostr(start, 10, true);
		write(1, fmt, ft_strlen(fmt));
	}
	write(1, "\e[2m ... \e[0;35m", 16);
	{
		char *fmt = ft_ultostr(end, 10, true);
		write(1, fmt, ft_strlen(fmt));
	}
	write(1, "\e[0;2m] = {\e[0m\n", 15 + newline);
}

static inline void		_write_tail(
		const uint8_t pad,
		const bool last
)
{
	_write_padding(pad);
	write(1, "\e[0;2m}", 7);
	if (!last)
		write(1, ",", 1);
	write(1, "\e[0m\n", 5);
}

static inline void		_print_block(
		const t_block * const block,
		const size_t index,
		const bool last
)
{
	_write_head(8, "Block", "\e[0;33m", index, false);
	_write_member_ptr(0, "ptr", (uintptr_t)block, false, false);
	_write_member_ptr(1, "mem", (uintptr_t)block->mem, false, false);
	_write_member_size(1, "offset", (uintptr_t)block->align_offset, false, false);
	_write_member_size(1, "size", (uintptr_t)block->size, false, true);
	_write_tail(0, last);
}

static inline void		_print_heap(
		const t_heap * const heap,
		const size_t index,
		const bool last
)
{
	static const char *const types[HEAP_MAX] = {
		[HEAP_NONE] = "\e[2mnone\e[0m",
		[HEAP_FAST] = "\e[32mfast\e[0m",
		[HEAP_SMALL] = "\e[33msmall\e[0m",
		[HEAP_LARGE] = "\e[38;5;166mlarge\e[0m",
	};
	_write_head(4, "Heap", "\e[0;32m", index, true);

	_write_span(6, (uintptr_t)heap, (uintptr_t)heap + heap->capacity);

	_write_member_bool(6, "empty", heap->flags.is_empty, true, false);
	_write_member_str(6, "type", types[heap->type], true, false);
	_write_member_ptr(6, "ptr", (uintptr_t)heap, true, false);
	_write_member_ptr(6, "mem", (uintptr_t)heap->mem, true, false);
	_write_member_ptr(6, "next", (uintptr_t)heap->next, true, false);
	_write_member_size(6, "used", (uintptr_t)heap->used, true, false);
	_write_member_size(6, "capacity", (uintptr_t)heap->capacity + sizeof(t_heap), true, false);
	_write_member_empty(6, "blocks", false);
	write(1, " \e[2m{\e[0m\n", 10 + !!heap->block);
	size_t block_index = 0;
	for (t_block *block = heap->block; block; block = block->next)
		_print_block(block, block_index++, !block->next);
	_write_tail((heap->block) ? 6 : 0, true);
	_write_tail(4, last);
}

static inline void		_print_bins(
		const t_bin *const bin,
		const size_t size
)
{
	ssize_t last_index_range = 0;

	for (size_t i = 0; i < size; ++i)
		if (bin[i].heap)
		{
			if (last_index_range != (ssize_t)i && last_index_range != -1)
			{
				if (last_index_range == (ssize_t)i - 1)
					_write_head(2, "Bin", "\e[0;34m", i - 1, false);
				else
					_write_head_range(2, "Bin", "\e[0;34m", last_index_range, i - 1, false);
				_write_tail(0, false);
			}
			last_index_range = -1;

			// Print bin contents
			_write_head(2, "Bin", "\e[0;34m", i, true);
			size_t index = 0;
			for (t_heap *heap = bin[i].heap; heap; heap = heap->next, index++)
				_print_heap(heap, index, !heap->next);
			_write_tail(2, (i == size - 1));
		}
		else if (last_index_range == -1)
			last_index_range = i;

	if (last_index_range > -1)
	{
		if (last_index_range == (ssize_t)size - 1)
			_write_head(2, "Bin", "\e[0;34m", last_index_range, false);
		else
			_write_head_range(2, "Bin", "\e[0;34m", last_index_range, size - 1, false);
		_write_tail(0, true);
	}
}

static inline void		_print_bin_type(
		const char * const name,
		const t_bin * const bins,
		const size_t size
)
{
	char				*str;

	write(1, name, strlen(name));
	write(1, "\e[2m[\e[0;34m", 12);
	str = ft_ultostr(_get_used_bins(bins, size), 10, true);
	write(1, str, ft_strlen(str));
	write(1, "\e[0;2m/\e[0;32m", 15);
	str = ft_ultostr(size, 10, true);
	write(1, str, ft_strlen(str));
	write(1, "\e[0;2m]\e[0m", 11);
	write(1, ":\n", 2);
	_print_bins(bins, size);
}

static inline void		_print_header(void)
{
	char				*str;

	write(1, "Malloc usage:\n  Allocated bytes: ", 33);
	str = ft_ultostr(g_mctx.allocated_bytes, 10, true);
	write(1, str, ft_strlen(str));
	write(1, "\n  Freed bytes: ", 16);
	str = ft_ultostr(g_mctx.freed_bytes, 10, true);
	write(1, str, ft_strlen(str));
	write(1, "\n  Page size: ", 14);
	str = ft_ultostr(g_mctx.page_size, 10, true);
	write(1, str, ft_strlen(str));
	write(1, "\n  Max allocation pages: ", 25);
	str = ft_ultostr(g_mctx.max / g_mctx.page_size, 10, true);
	write(1, str, ft_strlen(str));
	write(1, "\n", 1);
}

void					show_alloc_mem(void)
{
	_print_header();
	_print_bin_type("Fast", g_mctx.fast, sizeof(g_mctx.fast) / sizeof(t_bin));
	_print_bin_type("Small", g_mctx.small, sizeof(g_mctx.small) / sizeof(t_bin));
	_print_bin_type("Large", g_mctx.large, sizeof(g_mctx.large) / sizeof(t_bin));

	_print_bin_type("Freed", g_mctx.unsorted, 1);
}

void					show_block(
		const t_block * const block
)
{
	_print_block(block, 0, true);
}

void					show_heap(
		const t_heap * const heap
)
{
	_print_heap(heap, 0, true);
}

static inline void		find_ma_bin(
		const t_bin * const to_find,
		char ** const name,
		size_t * const index
)
{
	for (size_t i = 0; i < sizeof(g_mctx.fast) / sizeof(t_bin); ++i)
		if (g_mctx.fast + i == to_find) {
			*name = "Fast";
			*index = i;
			return;
		}
	for (size_t i = 0; i < sizeof(g_mctx.small) / sizeof(t_bin); ++i)
		if (g_mctx.small + i == to_find) {
			*name = "Small";
			*index = i;
			return;
		}
	for (size_t i = 0; i < sizeof(g_mctx.large) / sizeof(t_bin); ++i)
		if (g_mctx.large + i == to_find) {
			*name = "Large";
			*index = i;
			return;
		}
	*name = "Unkown";
	*index = 0;
}

void					show_bin(
		const t_bin * const bin
)
{
	char *name;
	size_t index;

	find_ma_bin(bin, &name, &index);

	// Print bin contents
	_write_head(0, name, "\e[0;34m", index, true);
	index = 0;
	for (t_heap *heap = bin->heap; heap; heap = heap->next, index++)
		_print_heap(heap, index, !heap->next);
	_write_tail(0, true);
}


