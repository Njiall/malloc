/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/17 03:42:03 by mbeilles          #+#    #+#             */
/*   Updated: 2021/08/24 10:34:36 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc_common.h"
#include "malloc_internal.h"

#include <unistd.h>
#include <sys/mman.h>
#include <string.h>

t_malloc_ctx	g_mctx = (t_malloc_ctx){
	.allocated_bytes = 0,
	.freed_bytes = 0,
	.max = 0,
	.page_size = 0,
};

// Assumes size can fit inside
t_bin			*ma_bin_from_block_size(
		size_t size
)
{
	size_t		index;
	struct range {
		size_t start;
		size_t end;
		size_t bin_length;
		t_bin *bins;
	} range;

	// Handle Fast types
	if (size <= TINY_SIZE_THRESHOLD)
		range = (struct range){
			.start = 1, .end = TINY_SIZE_THRESHOLD,
			.bin_length = sizeof(g_mctx.fast) / sizeof(*g_mctx.fast),
			.bins = g_mctx.fast,
		};
	else if (size <= SMALL_SIZE_THRESHOLD)
		range = (struct range){
			.start = TINY_SIZE_THRESHOLD + 1, .end = SMALL_SIZE_THRESHOLD,
			.bin_length = sizeof(g_mctx.small) / sizeof(*g_mctx.small),
			.bins = g_mctx.small,
		};
	else
		range = (struct range){
			.start = SMALL_SIZE_THRESHOLD + 1, .end = g_mctx.max - 1,
			.bin_length = sizeof(g_mctx.large) / sizeof(*g_mctx.large),
			.bins = g_mctx.large
		};
	const size_t bin_nb = (size_t)(((float)(size - range.start)
				/ (float)(range.end - range.start)) * (range.bin_length - 1));
	return (range.bins + bin_nb);
}

void			*ma_large_allocate(
		size_t size
)
{
	t_heap			*heap;

	// Finds a heap in the freed bin and there is no space allocates a new one
	if (!(heap = ma_heap_recycle(size)))
		if (!(heap = ma_heap_new(size)))
			// Failed allocation means we don't have ressources left
			return (NULL);
	ma_heap_push_front(&ma_bin_from_block_size(size)->heap, heap);
	t_block *new = ma_block_allocate(heap, size);
	return ((void*)new->mem + new->align_offset);
}

#include <stdio.h>
#include <sys/resource.h>
void			*ma_lloc(
		size_t size
)
{
	// Handle stupid sizes
	if (size == 0 || size >= g_mctx.max)
		return (NULL);
	// Find the right size for allocation
	else if (size > SMALL_SIZE_THRESHOLD)
		return (ma_large_allocate(size));
	return (ma_block_new(&ma_bin_from_block_size(size)->heap, size));
}

void			*calloc(
		size_t size,
		size_t count
)
{
	void *addr;
	if (!(addr = malloc(size * count)))
		return (NULL);
	ft_memset(addr, 0, size * count);
	return (addr);
}
