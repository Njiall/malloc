/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   heap_find_fit.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/21 10:56:07 by mbeilles          #+#    #+#             */
/*   Updated: 2020/01/31 23:15:46 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc_internal.h"

t_heap			*ma_heap_find_fit(
		t_heap *head,
		size_t size
)
{
	// Find free chunk in heaps
	while (head && head->capacity - head->used < size + 15 + sizeof(t_block))
		head = head->next;
	return (head);
}
