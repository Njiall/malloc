/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   block_new.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/21 10:52:47 by mbeilles          #+#    #+#             */
/*   Updated: 2021/08/24 10:33:18 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc_internal.h"

/// Finds the last block on the heap
/// Returns null if empty
t_block			*ma_block_find_last(
		const t_heap * const heap
)
{
	t_block		*last;

	last = heap->block;
	while (last && last->next)
		last = last->next;
	return (last);
}

/// Assumes that there is enough space in the heap
t_block			*ma_block_allocate(
		t_heap * const heap,
		size_t size
)
{
	t_block		*prev;
	t_block		*new;

	// Points at the start of block zone
	prev = NULL;
	// Find the last block on heap
	if (!heap->flags.is_empty)
		// Points after last block on heap
		if ((prev = ma_block_find_last(heap)))
			new = (void*)prev->mem + prev->size;
		else
			new = heap->block;
	else
	{
		heap->flags.is_empty = false;
		// If page is fresh we have to point right after
		new = heap->mem;
	}
	// Calculate for user data alignment to 16 bytes.
	// We can only creepup to the end to the page, so add to the address.
	// Or else we run the risk of writting to the end of the user's data
	// 		or the heap header.
	size_t offset = (16 - ((size_t)new->mem % 16)) % 16;
	new = (void*)new + offset;
	// Push back
	if (prev)
		// Init last block pointer to point to new one
		prev->next = new;
	else
		// Init page block pointer as it's empty
		heap->block = new;
	// Init block data
	*new = (t_block){.size = size, .align_offset = offset};
	// Show consuption of the heap
	heap->used += sizeof(t_block) + size;
	g_mctx.allocated_bytes += size;
	return (new);
}

t_heap			*ma_heap_recycle(
		const size_t size
)
{
	t_heap_type	type;
	t_heap *needle;

	// Get the type of heap to recycle
	if (size <= TINY_SIZE_THRESHOLD)
		type = HEAP_FAST;
	else if (size <= SMALL_SIZE_THRESHOLD)
		type = HEAP_SMALL;
	else
		type = HEAP_LARGE;

	// Indirect points to the pointer to update in the list.
	// Could be the head, could be any pointer of the list.
	t_heap **indirect = &g_mctx.unsorted[0].heap;
	// Walk the list until we get the an entry searched.
	while (*indirect
			&& !((*indirect)->type == type && (*indirect)->capacity > size + 15))
		indirect = &(*indirect)->next;
	// Store found entry, can be null
	needle = *indirect;
	// Could be null removes the entry from the list
	if (*indirect)
		*indirect = (*indirect)->next;
	return (needle);
}
#include "libft.h"

void			*ma_block_new(
		t_heap ** const head,
		const size_t size
)
{
	t_heap	*needle;

	printf("Block new\n");
	// Find free chunk in heaps
	if (!(needle = ma_heap_find_fit(*head, size)))
	{
		// Try to find a recyclable heap from the unsorted list
		if (!(needle = ma_heap_recycle(size)))
			// Allocates a new heap for storing new chunk
			if (!(needle = ma_heap_new(size)))
				return (NULL);
		// Push front as the new collected is not in the right bin
		ma_heap_push_front(head, needle);
	}
	printf("Found free\n");
	t_block	*new = ma_block_allocate(needle, size);
	// returns allocaed heap
	return ((void*)new->mem);
}
