/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   heap_allocate.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/21 11:00:45 by mbeilles          #+#    #+#             */
/*   Updated: 2020/09/23 05:47:56 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc_internal.h"
#include <unistd.h>
#include <sys/mman.h>

// Defining page size for tiny to be depend on the threshold
#if TINY_SIZE_THRESHOLD < (4096 * 4)
#define TINY_SIZE 4
#else
#define TINY_SIZE (TINY_SIZE_THRESHOLD / (4096 * 4))
#if TINY_SIZE < 4
#define TINY_SIZE 4
#endif
#endif
// Defining page size for small to be depend on the threshold
#if SMALL_SIZE_THRESHOLD < (4096 * 16)
#define SMALL_SIZE 16
#else
#define SMALL_SIZE (SMALL_SIZE_THRESHOLD / (4096 * 16))
#if SMALL_SIZE < 16
#define SMALL_SIZE 16
#endif
#endif

static inline size_t	_get_heap_size(
		size_t size
)
{
	size_t		physical_size;

	switch (size) {
		case 0 ... TINY_SIZE_THRESHOLD:
			return (TINY_SIZE);
		case TINY_SIZE_THRESHOLD + 1 ... SMALL_SIZE_THRESHOLD:
			return (SMALL_SIZE);
		default:
			// Gets the size for the whole structure to fit
			physical_size = size + sizeof(t_heap) + sizeof(t_block) + 15;
			// Find power of 2 rounded up of size
			physical_size--;
			physical_size |= physical_size >> 1;
			physical_size |= physical_size >> 2;
			physical_size |= physical_size >> 4;
			physical_size |= physical_size >> 8;
			physical_size |= physical_size >> 16;
			physical_size++;
			// Since heap size is multiple of page size
			physical_size /= getpagesize();
			if (physical_size < 1)
				physical_size = 1;
			return (physical_size);
	};
}

static inline t_heap_type	_get_heap_type(size_t size)
{
	switch (size) {
		case 0 ... TINY_SIZE_THRESHOLD:
			return (HEAP_FAST);
		case TINY_SIZE_THRESHOLD + 1 ... SMALL_SIZE_THRESHOLD:
			return (HEAP_SMALL);
		default:
			return (HEAP_LARGE);
	}
}

#include "libft.h"
t_heap					*ma_heap_new(
		size_t size
)
{
	const size_t		page_size = getpagesize();
	size_t				heap_size;
	t_heap				*adr;


	heap_size = _get_heap_size(size);
	adr = mmap(
			NULL,
			page_size * heap_size,
			PROT_READ | PROT_WRITE | PROT_EXEC,
			MAP_ANON | MAP_PRIVATE,
			-1, 0
	);
	if (adr == MAP_FAILED)
		return (NULL);
	*adr = (t_heap){
		.flags = (t_heap_flag){
			.is_empty = true,
		},
		.type = _get_heap_type(size),
		.capacity = heap_size * page_size - sizeof(t_heap),
		.next = NULL,
	};
	/*ft_putendl(ft_ultostr((adr->capacity * page_size + sizeof(t_heap)) / page_size, 10, true));*/
	/*print_heap_zone(adr);*/
	return (adr);
}
