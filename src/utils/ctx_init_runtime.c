/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ctx_init_runtime.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/04 22:55:22 by mbeilles          #+#    #+#             */
/*   Updated: 2021/08/24 10:28:47 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <sys/resource.h>
#include "malloc_internal.h"

static void con() __attribute__((constructor));
void con() {
	printf("Initializing constructor\n");
	ma_ctx_init_runtime();
}

void			ma_ctx_init_runtime(void)
{
	struct rlimit	rlim;

	if (g_mctx.max == 0 && g_mctx.page_size == 0)
	{
		getrlimit(RLIMIT_MEMLOCK, &rlim);
		g_mctx.page_size = getpagesize();
		g_mctx.max = rlim.rlim_cur * g_mctx.page_size;
	}
}
