/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   heap_push_front.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/21 11:06:22 by mbeilles          #+#    #+#             */
/*   Updated: 2020/01/23 11:24:43 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc_internal.h"

void	ma_heap_push_front(
		t_heap ** head,
		t_heap * elem
)
{
	elem->next = (*head);
	*head = elem;
}
