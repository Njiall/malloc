/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_heap_zone.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/21 13:42:26 by mbeilles          #+#    #+#             */
/*   Updated: 2020/07/27 17:52:30 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc_internal.h"
#include "libft.h"
#include <unistd.h>
#include <stdio.h>

typedef enum		e_zone_type
{
	ZONE_NONE,
	ZONE_HD_HEAP,
	ZONE_HD_BLOCK,
	ZONE_USER,
	ZONE_PAD,
	ZONE_MAX
}					t_zone_type;

static char			*g_colors[ZONE_MAX] = {
	[ZONE_PAD] = "\e[0;2;37m",
	[ZONE_HD_HEAP] = "\e[1;34m",
	[ZONE_HD_BLOCK] = "\e[1;33m",
	[ZONE_USER] = "\e[1;32m",
	[ZONE_NONE] = "\e[0m",
};

void DumpHex(const void* data, size_t size) {
	char ascii[17];
	size_t i, j;
	ascii[16] = '\0';
	for (i = 0; i < size; ++i) {
		printf("%02X ", ((unsigned char*)data)[i]);
		if (ft_isprint(((uint8_t*)data)[i])) {
			ascii[i % 16] = ((unsigned char*)data)[i];
		} else {
			ascii[i % 16] = '.';
		}
		if ((i+1) % 8 == 0 || i+1 == size) {
			printf(" ");
			if ((i+1) % 16 == 0) {
				printf("|  %s \n", ascii);
			} else if (i+1 == size) {
				ascii[(i+1) % 16] = '\0';
				if ((i+1) % 16 <= 8) {
					printf(" ");
				}
				for (j = (i+1) % 16; j < 16; ++j) {
					printf("   ");
				}
				printf("|  %s \n", ascii);
			}
		}
	}
}

static inline bool	skip_block(
		const void * const zone,
		const size_t index,
		const size_t size
)
{
	if (index + 32 <= size)
	{
		int j;
		for (j = 0; (((uint8_t*)zone)[index + j] == 0x00) && j < 32; ++j);
		if (j == 32)
			return (true);
	}
	return (false);
}

static inline bool	print_zone(
		const void * const zone,
		const size_t size
)
{
	char ascii[33];
	size_t i, j;
	bool first = true;

	ascii[32] = '\0';
	for (i = 0; i < size; ++i)
	{
		if (i % 32 == 0) {
			size_t start = i;
			j = 0;
			for (; skip_block(zone, i, size); i += 32)
				++j;
			if (j > 1) {
				printf("\e[2;34m0x\e[0;34m%llx\e[2m: \e[0;1;34m*\e[0m\n", (uint64_t)(zone + start));
			} else {
				i = start;
			}
		}
		if (i % 16 == 0)
			printf("\e[2;34m0x\e[0;34m%llx\e[2m:\e[0m ", (uint64_t)zone + i);
		if (!((uint8_t*)zone)[i])
			printf("\e[2;37m00\e[0m ");
		else if (((uint8_t*)zone)[i] == 0xbe)
			printf("\e[2;38;5;166mbe\e[0m ");
		else
			printf("\e[1m%02x\e[0m ", ((uint8_t*)zone)[i]);
		if (ft_isprint(((uint8_t*)zone)[i])) {
			ascii[i % 32] = ((uint8_t*)zone)[i];
		} else {
			ascii[i % 32] = '.';
		}
		if ((i+1) % 16 == 0 || i+1 == size) {
			if ((i+1) % 32 == 0) {
				printf("\e[2m| \e[0;34m%.16s %s\e[0m\n", ascii, ascii + 16);
			} else if (i+1 == size) {
				ascii[(i+1) % 32] = '\0';
				if ((i+1) % 32 <= 16) {
					printf(".");
				}
				for (j = (i+1) % 32; j < 32; ++j) {
					printf("   ");
				}
				if ((i+1) % 32 > 16)
					printf("\e[2m| \e[0;34m%.16s %s\e[0m\n", ascii, ascii + 16);
				else
					printf("\e[2m| \e[0;34m%s\e[0m\n", ascii);
			} else {
				printf("\e[2m|\e[0m ");
			}
		}
		first = true;
	}
	return (true);
}

void			print_heap_zone(const t_heap * const heap)
{
	print_zone(heap, heap->capacity);
	/*DumpHex(heap, heap->capacity + sizeof(t_heap));*/
	/*for (size_t i = 0; i < heap->capacity + sizeof(t_heap); ++i)*/
	/*{*/
		/*if (i % 32 == 0)*/
			/*print_line_hd((void*)heap + i);*/
		/*if (i % 32 == 31)*/
			/*write(1, "\n", 1);*/
	/*}*/
}
