/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   realloc.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/23 13:31:33 by mbeilles          #+#    #+#             */
/*   Updated: 2020/07/30 19:34:51 by njiall           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc_internal.h"
#include "libft.h"

inline t_heap_type		ma_type_get(
		size_t size
)
{
	if (size <= TINY_SIZE_THRESHOLD)
		return (HEAP_FAST);
	else if (size <= SMALL_SIZE_THRESHOLD)
		return (HEAP_SMALL);
	else
		return (HEAP_LARGE);
}

void					*ma_alloc_move(
	const t_bin *const head,
	t_heap *heap,
	t_block *block,
	size_t size
)
{
	void *new = ma_block_new(&ma_bin_from_block_size(size)->heap, size);
	if (block->size < size)
		ft_memcpy(new, block->mem, block->size);
	else
		ft_memcpy(new, block->mem, size);
	// Removes the block and tries to dispose of the bin
	if (heap && block)
		// If the remove left us an empty heap, then we recycle it
		if (ma_block_remove(heap, block))
			ma_heap_dispose((t_bin*)head, heap);
	return (new);
}

void					*realloc(
		void *elem,
		size_t size
)
{
	const t_bin			*head;
	t_heap				*heap;
	t_block				*block;
	t_heap_type			type = ma_type_get(size);

	if (!elem)
		return (malloc(size));
	// Handle stupid sizes
	if (!size || size >= g_mctx.max
			|| !ma_heap_block_from_elem(elem, &head,
			(const t_heap **)&heap,
			(const t_block **)&block))
		return (NULL);
	if (size == block->size) // Why even the fuck would you do that?
		return (elem);

	// Calculate correctly the size of the allocated space.
	g_mctx.allocated_bytes -= block->size;
	g_mctx.allocated_bytes += size;

	// Detect if we can just virtually extend the size
	if (head == ma_bin_from_block_size(size) && heap->capacity > size)
	{
		heap->used -= block->size;
		heap->used += size;
		block->size = size;
	}
	else
		// Detect if we should move allocate
		elem = ma_alloc_move(head, heap, block, size);
	return (elem);
}
