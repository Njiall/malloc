/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/19 11:08:17 by mbeilles          #+#    #+#             */
/*   Updated: 2021/08/24 10:26:52 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc_internal.h"
#include <stddef.h>
#include <assert.h>
#include <sys/mman.h>

const t_heap		*ma_block_find_heap(
		const t_heap * const head,
		const t_block * const block
)
{
	const t_heap *needle = head;
	while (needle)
	{
		// TODO Efficiency refactor
		const t_block *stack = block;
		if (needle->mem <= block
				&& ((void*)block + block->size <= (void*)needle + needle->capacity + sizeof(t_heap)))
		{
			stack = needle->block;
			while (stack)
				if (stack == block)
					return (needle);
				else
					stack = stack->next;
			return (NULL);
		}
		needle = needle->next;
	}
	return (NULL);
}

bool			ma_heap_block_from_elem(
		void *elem,
		const t_bin **bin,
		const t_heap **heap,
		const t_block **block
)
{
	// Gets block header from element
	*block = elem - sizeof(t_block);
	// Find the element from it's size
	// TODO Refactor to search from block size and unsure approach
	*bin = ma_bin_from_block_size((*block)->size);
	*heap = ma_block_find_heap((*bin)->heap, *block);
	return (*heap && *block);
}

const t_block		*ma_block_find_prev(
		const t_heap * const heap,
		const t_block * const block
)
{
	const t_block *needle = heap->block;
	// TODO Efficiency refactor
	while (needle)
		if (needle->next == block)
			return (needle);
		else
			needle = needle->next;
	return (NULL);
}

bool			ma_block_remove(
		t_heap * const heap,
		const t_block * const block
)
{
	t_block			*prev;
	bool			empty;

	heap->used -= block->size + sizeof(t_block);

	g_mctx.allocated_bytes -= block->size;
	g_mctx.freed_bytes += block->size;

	prev = (t_block*)ma_block_find_prev(heap, block);
	empty = !prev && !block->next;
	if (prev)
		prev->next = block->next;
	else
		heap->block = block->next;
	return (empty);
}

void			ma_heap_remove(
		t_bin *const haystack,
		const t_heap *const needle
)
{
	t_heap		**indirect;

	// Indirect points to the pointer to update in the list.
	// Could be the head, could be any pointer of the list.
	indirect = &haystack->heap;
	// Walk the list until we get the entry.
	while (*indirect != needle)
		indirect = &(*indirect)->next;
	// Remove entry from the list as `indirect` is the pointer `next`
	// of the entry before our needle.
	// `indirect` <=> `&prev->next` for example.
	// But we avoid branching un-necessarily.
	*indirect = needle->next;
}

void			ma_heap_dispose(
		t_bin *head,
		t_heap *heap
)
{
	ma_heap_remove((t_bin*)head, heap);
	heap->block = NULL;
	heap->flags.is_empty = true;
	ma_heap_push_front(&g_mctx.unsorted[0].heap, heap);
}

void			fr_ee(
		void *elem
)
{
	const t_bin			*head;
	const t_block		*block;
	t_heap				*heap;
	bool				found;

	// If we found the block and heap targeted, we clear it from the heap
	if (elem && ma_heap_block_from_elem(elem, &head, (const t_heap **)&heap, &block))
	{
		// If the remove left us an empty heap, then we recycle it
		if (ma_block_remove(heap, block))
			ma_heap_dispose((t_bin*)head, heap);
	}
}
