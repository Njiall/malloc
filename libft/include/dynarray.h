/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dynarray.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/15 20:09:51 by mbeilles          #+#    #+#             */
/*   Updated: 2020/01/23 13:08:02 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DYNARRAY_H
# define DYNARRAY_H

# include <stdint.h>
# include <stdbool.h>

# define DYNARRAY_DEFAULT_SIZE (1u << 11u)
# define DYNARRAY_DEFAULT_MAX (1u << 23u)

typedef struct		s_dynarray {
	uint8_t			*array;
	uint64_t		offset;
	uint64_t		index;
	uint64_t		size;
	uint64_t		max_size;
	uint64_t		min_size;
}					t_dynarray;

/*
** =============================================================================
** 				Array creation
** =============================================================================
**
** Allocates the array structure
*/
t_dynarray			*ft_dynarray_create(
		const uint64_t min_size,
		const uint64_t max_size
);
/*
** Returns the array structure
*/
t_dynarray			ft_dynarray_create_loc(
		const uint64_t min,
		const uint64_t max
);
/*
** Duplicate array structure and contents
*/
t_dynarray			*ft_dynarray_clone(
		const t_dynarray * const arr
);
/*
** Duplicate array structure and contents
*/
t_dynarray			ft_dynarray_clone_loc(
		const t_dynarray * const arr
);
/*
** Manual resize of physical array
*/
t_dynarray			*ft_dynarray_resize(
		t_dynarray *arr,
		const uint64_t size
);

/*
** =============================================================================
** 				Stack manipulation
** =============================================================================
**
** Pushes the element at the end of the array
*/
void				*ft_dynarray_push(
		t_dynarray *arr,
		const void * constelem,
		const uint64_t size
);
/*
** Pops the last element out of a dynarray.
** 		It stores it in the copy parameter and asumes the copy location can
** 		stores the data since allocating is a bad idea (performance wise).
*/
bool				ft_dynarray_pop(
		t_dynarray *arr,
		void * const cpy,
		const uint64_t size
);
/*
** Gets the element on the top of the stack.
** 		Returns NULL if failed.
*/
void					*ft_dynarray_stack_top(
		t_dynarray *arr,
		const uint64_t size
);
/*
** Gets the element on the bottom of the stack.
** 		Returns NULL if failed.
*/
void					*ft_dynarray_stack_bottom(
		t_dynarray *arr,
		const uint64_t size
);

/*
** =============================================================================
** 				Queue manipulation
** =============================================================================
**
** Pushes an element at the end of the queue
*/
void				*ft_dynarray_queue(
		t_dynarray *arr,
		const void * const elem,
		const uint64_t size
);
/*
** Pops the first element out of a dynarray.
** 		It stores it in the copy parameter and asumes the copy location can
** 		stores the data since allocating is a bad idea (performance wise).
*/
bool				ft_dynarray_unqueue(
		t_dynarray *arr,
		void * const cpy,
		const uint64_t size
);
/*
** Gets the element on the top of the queue.
** 		Returns NULL if failed.
*/
void					*ft_dynarray_queue_top(
		t_dynarray *arr,
		const uint64_t size
);
/*
** Gets the element on the bottom of the queue.
** 		Returns NULL if failed.
*/
void					*ft_dynarray_queue_bottom(
		t_dynarray *arr,
		const uint64_t size
);

/*
** =============================================================================
** 				Array manipulation
** =============================================================================
**
** Sets an element at a given index in a dynarray.
*/
void				*ft_dynarray_set(
		t_dynarray *arr,
		const uint64_t index,
		const void * const elem,
		const uint64_t size
);
/*
** Gets an element at a given index in a dynarray.
*/
void				*ft_dynarray_get(
		t_dynarray *arr,
		const uint64_t index,
		const uint64_t size
);
/*
** Inserts an element at a given index in a dynarray.
*/
void				*ft_dynarray_insert(
		t_dynarray *arr,
		const uint64_t index,
		const void * const elem,
		const uint64_t size
);

/*
** =============================================================================
** 				Array removal
** =============================================================================
**
** Removes the specified array zone
*/
bool				ft_dynarray_remove(
		t_dynarray *arr,
		const uint64_t index,
		const uint64_t size
);
/*
** Yanks an item out of the array and returns it
*/
bool				ft_dynarray_yank(
		t_dynarray *arr,
		const uint64_t index,
		void * const cpy,
		const uint64_t size
);
/*
** Empties the arrays contents
*/
void				*ft_dynarray_purge(
		t_dynarray *arr
);
/*
** Frees allocted zones in array
*/
void				ft_dynarray_destroy(
		const t_dynarray * const arr,
		const bool is_alloc
);

/*
** =============================================================================
** 				Miscs
** =============================================================================
** Array shorthand for pushing strings
*/
void				*ft_dynarray_push_str(
		t_dynarray *arr,
		const char * const str
);
/*
** Duplicate only the zone used and appends a `\0` at the end
*/
char				*ft_dynarray_to_str(
		const t_dynarray * const arr
);
/*
** Prints dynarray contents as hexdump
*/
void				ft_dynarray_print(
		const t_dynarray * const arr
);
/*
** Permits iteration over array zones
** E.g.:
**
** uint64_t			iter = 0;
** void				*zone;
**
** while ((zone = ft_dynarray_iterate(arr, &iter, sizeof(*zone)))) {
** 	// Do watever
** }
*/
void				*ft_dynarray_iterate(
		const t_dynarray * const arr,
		uint64_t *iter,
		const uint64_t esize
);
void				*ft_dynarray_iterate_rev(
		const t_dynarray * const arr,
		uint64_t *iterator,
		const uint64_t size
);

t_dynarray			*ft_dynarray_from_fd(int fd);

#endif
