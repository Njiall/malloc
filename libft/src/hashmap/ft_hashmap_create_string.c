/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hashmap_create_id_string.c                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/27 11:20:15 by mbeilles          #+#    #+#             */
/*   Updated: 2019/10/27 11:22:52 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "hashmap.h"
#include "libft.h"

static t_hash_type		(*g_hash[HASH_FUNC_MAX])(const void *, const size_t) = {
	[HASH_FUNC_DJB2] = ft_hash_djb2,
	[HASH_FUNC_DJB2A] = ft_hash_djb2a,
	[HASH_FUNC_FNV1A] = ft_hash_fnv1a,
};

t_hashmap				*ft_hashmap_create_id_string(
		t_hash_function_id id
)
{
	t_hashmap		*map;

	if (!(map = (t_hashmap*)malloc(sizeof(t_hashmap))))
		return (NULL);
	map->length = 0;
	if (id >= 0 && id < HASH_FUNC_MAX && g_hash[id])
		map->hash_function = g_hash[id];
	else
		map->hash_function = g_hash[HASH_FUNC_DEFAULT];
	map->size_evaluator = &ft_hash_strlen;
	ft_memset(map->table, 0, sizeof(*map->table) * HASHTABLE_SIZE);
	return (map);
}

t_hashmap				ft_hashmap_create_id_string_loc(
		t_hash_function_id id
)
{
	t_hashmap			map;

	map = (t_hashmap){
			.size_evaluator = &ft_hash_strlen,
	};
	if (id >= 0 && id < HASH_FUNC_MAX && g_hash[id])
		map.hash_function = g_hash[id];
	else
		map.hash_function = g_hash[HASH_FUNC_DEFAULT];
	return (map);
}
