#include <string.h>
#include <criterion/criterion.h>
#include <criterion/parameterized.h>
#include <criterion/logging.h>

#include "dynarray.h"
#include "ft_tests.h"

typedef struct	s_test {
	// Display
	char		desc[4096];

	// Outputs
	uint8_t		expected[4096];
	uint64_t	elen;

	// Variables
	size_t		index;
	uint64_t	dlen;

	// Contents
	uint8_t		contents[4096];
	uint64_t	clen;
	uint8_t		residue[4096];
	uint64_t	rlen;

	bool		invalid;
}				t_test;

ParameterizedTestParameters(dynarray, yank) {
	static t_test			tests[] = {
		(t_test){
			.desc = "yank at 0 on empty array",

			.index = 0,

			.dlen = 1,
			.invalid = true,
		},
		(t_test){
			.desc = "yank at 5 of 0x22.",

			.index = 5,

			.expected = 	{[0] = 0x22},
			.contents = 	{[0 ... 4] = 0x10, [5] = 0x22},
			.residue = 		{[0 ... 4] = 0x10},
			.elen = 1,
			.dlen = 1,
			.clen = 6,
			.rlen = 5,
		},
		(t_test){
			.desc = "yank at 128 of 0x1010101010 with contents of 0x55 on 128 bytes",

			.index = 128,

			.expected = 	{[0 ... 5] = 0x10},
			.contents = 	{[0 ... 127] = 0x55, [128 ... 133] = 0x10},
			.residue = 		{[0 ... 127] = 0x55},
			.elen = 6,
			.dlen = 6,
			.clen = 134,
			.rlen = 128,
		},
	};

	// generate parameter set
	return cr_make_param_array(t_test, tests, sizeof(tests) / sizeof(t_test));
}

ParameterizedTest(t_test *test, dynarray, yank)
{
	// Init
	t_dynarray		output = ft_dynarray_create_loc(0, 0);
	uint8_t			data[4096] = {};
	// Fills contents
	if (test->clen > 0)
		ft_dynarray_push(&output, test->contents, test->clen);

	// Run test
	bool fetched = ft_dynarray_yank(&output, test->index, data, test->dlen);

	// Gets results
	bool match_valid = !test->invalid == fetched;
	bool match_content = !memcmp(
			data,
			test->expected,
			test->elen
	);
	bool match_len = output.index == test->rlen;
	bool match_residue = !memcmp(
			output.array + output.offset,
			test->residue,
			output.index
	);

	// Handle results
	cr_expect(match_valid, "%s\nReturn states doesn't match, out: %s != exp: %s",
			test->desc,
			fetched ? "true" : "false", test->invalid ? "false" : "true");
	cr_expect(match_len, "%s\nResidue size doesn't match, out: %llu != exp: %llu",
			test->desc,
			output.index, test->rlen);
	cr_expect(match_content, "%s\nContent doesn't match:\n%s",
				test->desc,
				get_mem_diff(
					data,
					test->expected,

					test->dlen,
					test->elen
				)
	);
	cr_expect(match_residue, "%s\nResidue content doesn't match:\n%s",
			test->desc,
			get_mem_diff(
				output.array + output.offset,
				test->residue,

				output.index,
				test->rlen
			)
	);

	// Cleanup
	ft_dynarray_destroy(&output, false);
}
