/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc_internal.h                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/17 03:48:33 by mbeilles          #+#    #+#             */
/*   Updated: 2020/07/27 12:06:34 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MALLOC_INTERNAL_H
# define MALLOC_INTERNAL_H

# include <stddef.h>
# include <stdint.h>
# include <stdbool.h>

/*
** =============================================================================
** 			Heap meta data
** =============================================================================
**
** Malloc bins size:
** 		 - Fast:  4 pages
** 		 - Small: 16 pages
** 		 - Large: * pages (detemined by the mutiple of pages closer to size)
**
** =============================================================================
*/

# define MALLOC_START_ADDRESS		(1 << 0)

# define TINY_SIZE_THRESHOLD		88
# define SMALL_SIZE_THRESHOLD		2048

typedef enum			e_heap_type
{
	HEAP_NONE,
	HEAP_FAST,
	HEAP_SMALL,
	HEAP_LARGE,
	HEAP_MAX
}						t_heap_type;

typedef struct			s_heap_flag
{

	bool				is_empty: 1;
	bool				is_fractured: 1;

}						t_heap_flag;

/*
** =============================================================================
** 			Heap structures
** =============================================================================
*/

typedef struct			s_block
{
	size_t				size;
	size_t				align_offset;
	struct s_block		*next;
	// Comment and uncomment to test padding on memory
	//int					pad;

	// Points to what's after, namely user data
	// Equivalent to (void*)block + sizeof(t_block)
	void				*mem[0];
}						t_block;

typedef struct			s_heap
{
	t_heap_type			type: 2;
	t_heap_flag			flags;

	size_t				capacity;
	size_t				used;
	t_block				*block;
	struct s_heap		*next;

	// Points to what's after, namely the block data
	t_block				mem[0];
}						t_heap;

typedef struct			s_bin
{
	t_heap_type			type;
	t_heap				*heap;
	size_t				count;
}						t_bin;

/*
** =============================================================================
** 			Context structure
** =============================================================================
**
** This is the context used throughout the library as an indexing of memory
** 		locations with their data an other.
*/

typedef struct			s_malloc_ctx
{
	size_t				allocated_bytes;
	size_t				freed_bytes;
	size_t				page_size;
	size_t				page_allocated;
	size_t				max;

	t_bin				fast[10];
	t_bin				small[64];
	t_bin				large[64];

	t_bin				unsorted[1];
}						t_malloc_ctx;

void					ma_ctx_init_runtime(void);

extern t_malloc_ctx		g_mctx;

/*
** =============================================================================
** 			Function prototypes
** =============================================================================
** Used for internal handling of the allocation and freeing proccess.
**
** /!\ Note:
** 		Large chunk are not (usually) recycled as they take too much memory.
** 		But they can be so heaps should be chosen wisely.
**
** =============================================================================
**
** Heap collection
** Collects any empty heap and push it front into the unsorted heap
*/
inline void				ma_collect_all(void);
/*
** Collects an empty heap and push it front into the unsorted heap
*/
inline void				ma_collect_zone(void *zone);
/*
** Collects any empty heap and push it front if needed, otherwise freed them.
** This is best used to keep memory consuption low.
*/
inline void				ma_smart_collect_all(void);
/*
** Finds the chunk and bins corresponding to given addess
*/
inline bool				ma_find_elem(void *elem, t_heap **heap, void **chunk);
/*
** Get the right bin for the asked size
*/
t_bin					*ma_bin_from_block_size(
		size_t size
);

/*
** Heap allocation
** Allocates an chunk in the fast bin an returns the address of it.
*/
inline void				*ma_fast_allocate(size_t size);
/*
** Allocates an chunk in the small bin an returns the address of it.
*/
inline void				*ma_small_allocate(size_t size);
/*
** Allocates an chunk in the large bin an returns the address of it.
*/
inline void				*ma_large_allocate(size_t size);

/*
** Mostly used for large allocations
** Finds or allocates new heap if needed
*/
t_heap					*ma_heap_get(
		t_heap *const head,
		size_t elem_size,
		size_t page_mult
);
/*
** Finds the heap of given block
*/
const t_heap			*ma_block_find_heap(
		const t_heap * const head,
		const t_block * const block
);
/*
** push front a new heap into it
*/
t_heap					*ma_heap_new(
		const size_t size
);
/*
** Removes the needle from the bin heaystack
*/
void					ma_heap_remove(
		t_bin *const haystack,
		const t_heap *const needle
);

/*
** Used for fast and small allocations
** Allocates a new block on heap list, if there is no space,
** push front a new heap into it
*/
void					*ma_block_new(
		t_heap **const head,
		const size_t size
);
/*
** Finds a heap in list that can fit size
*/
t_heap					*ma_heap_find_fit(
		t_heap *const head,
		size_t elem_size
);
/*
** Allocates a block on the given heap and returns it
*/
t_block					*ma_block_allocate(
		t_heap *const heap,
		size_t size
);
/*
** Removes a heap from the bin and disposes it into the unsorted bin
*/
void			ma_heap_dispose(
		t_bin *head,
		t_heap *heap
);
/*
** Pushes the element to head's pointer and links to it
*/
void					ma_heap_push_front(
		t_heap **head,
		t_heap *elem
);
/*
** Get a recyclable heap from the unsorted bin.
** Given the size it will select a previously typed heap
*/
t_heap					*ma_heap_recycle(
		const size_t size
);
/*
** Gets every information on an element
*/
bool					ma_heap_block_from_elem(
		void *elem,
		const t_bin **bin,
		const t_heap **heap,
		const t_block **block
);

/*
** Removes a chunk from the heap and returns if the heap is empty
*/
bool					ma_block_remove(
		t_heap *const heap,
		const t_block *const block
);
/*
** Copies a chunk onto an other one
*/
void					ma_block_copy(
		const void *const dst,
		const void *const src
);
t_heap_type				ma_type_get(
		size_t size
);

void					print_heap_zone(
		const t_heap *const heap
);
void					show_block(
		const t_block *const block
);
void					show_heap(
		const t_heap *const heap
);
void					show_bin(
		const t_bin *const bin
);

#endif
