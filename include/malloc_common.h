/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc_common.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/17 03:48:33 by mbeilles          #+#    #+#             */
/*   Updated: 2021/08/24 10:26:43 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MALLOC_COMMON_H
# define MALLOC_COMMON_H

#include <stddef.h>

// Allocation functions
void			*ma_lloc(size_t size);
void			*realloc(void *mem, size_t new_size);
void			*calloc(size_t nb, size_t count);

void			show_alloc_mem(void);

// Cleaning functions
void			fr_ee(void *mem);

#endif
