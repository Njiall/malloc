#include <stdio.h>
#include <string.h>
#include "malloc_common.h"
/*#include <stdlib.h>*/

#include <unistd.h>
#include <stdlib.h>
#include <time.h>

void swap (char **a, char **b)
{
	char *temp = *a;
	*a = *b;
	*b = temp;
}

int random_between(int min, int max)
{
	return ((rand() % (max + 1 - min)) + min);
}

#include <limits.h>

void memread(volatile const void* const mem, const size_t len)
{
	char a;
	if (len >= 10) {
		size_t i = 0;
		printf("[");
		for (; i < 5; i++) {
			a = ((const char* const)mem)[i];
			printf("%hhx, ", a);
		}
		for (; i < len - 5; i++) {
			a = ((const char* const)mem)[i];
		}
		printf("... , ");
		for (; i < len - 1; i++) {
			a = ((const char* const)mem)[i];
			printf("%hhx, ", a);
		}
		a = ((const char *const)mem)[i];
		printf("%hhx]", a);
	} else {
		printf("[");
		size_t i = 0;
		for (; i < len - 1; i++) {
			a = ((const char* const)mem)[i];
			printf("%hhx, ", a);
		}
		a = ((const char* const)mem)[i];
		printf("%hhx]", a);
	}
}

int main(void)
{
	const int min = 10;
	const int max = 100;
	const int dim = 10;
	const int rounds = 1000;
	char *ptrs[dim][max - min];

	char str[256];
	srand (time(NULL));

	for (int round = 0; round < rounds; round++)
	{
		printf("Doing %i allocs\n", dim * (max - min));
		// Allocate pointers and verify memory was allocated
		for (int u = 0; u < dim; u++)
			for (int i = min; i < max; i++)
			{
				printf("Alloc\n");
				int len = random_between(10, 400);
				ptrs[u][i - min] = ma_lloc(len + 1); // Allocate memroy space
				printf("Write\n");
				memset(ptrs[u][i - min], 'W', len); // Tests the allocation space
				ptrs[u][i - min][len] = '\0';
				printf("Read: ");
				memread(ptrs[u][i - min], len + 1);
				printf(", len: %i\n", len + 1);
				printf("Done\n");
			}

		// Convert table pointer from 2D to 1D
		const int n = dim * (max - min);
		char *((*arr)[n]) = (char*(*)[n])(&ptrs);

		// Shuffle table to fr_ee it randomly
		for (int i = n - 1; i > 0; i--)
		{
			int j = rand() % (i + 1);
			swap(*arr + i, *arr + j);
		}

		// Batch fr_ee
		/*printf("Alloc Round[%i]\n", round);*/
		/*show_alloc_mem();*/
		for (int u = 0; u < dim; u++)
			for (int i = min; i < max; i++)
			{
				int len = i - min;
				fr_ee(ptrs[u][len]);
			}
		/*printf("fr_ee Round[%i]\n", round);*/
		/*show_alloc_mem();*/
	}
}
